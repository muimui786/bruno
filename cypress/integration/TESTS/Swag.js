describe('SWAG', function () {
   before(function(){
      cy.fixture('details').then(function(data){
         this.data=data
      })
   })

    
   it('Visit site and log in', function (){
        cy.visit('https://www.saucedemo.com/');
        cy.get('#user-name').type(this.data.username3);
        cy.get('#password').type(this.data.password3);
        cy.get('#login-button').click();
        cy.wait(3000);
   })

   it('Add products to cart', function (){
      cy.get('#add-to-cart-sauce-labs-backpack').click();
      cy.get('#add-to-cart-sauce-labs-bolt-t-shirt').click();
      cy.wait(10000);
   })

   it('View Cart & checkout', function (){
      cy.get('.shopping_cart_link').click();
      cy.wait(5000);
      //cy.get('checkout').click();
      //cy.wait(10000);
   })

   /*it('Complete form details', function (){
      cy.get('#first-name').type(this.data.fname);
      cy.get('#last-name').type(this.data.lname);
      cy.get('#postal-code').type(this.data.zip);
   })*/

})