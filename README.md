# BRUNO

Individual/group task provided by Ricky to automate few tasks on the webapp :: https://opensource-demo.orangehrmlive.com/

## Getting started

1. Clone this repository to your local
2. Make sure you have latest node.js installed
     - To verify your version, enter command: **node --version / node -v**
     - To install or update, run this command as ADMIN: **npm i**
3. Next, install cypress by running the below commands:
    - **npm init**
    - **npm install cypress --save-dev**
4. Start the Magic!

## Support
If you're having any issues, visit the following:
1. Cypress: [Installing Cypress](https://docs.cypress.io/guides/getting-started/installing-cypress#What-you-ll-learn)
2. Node: [Node versions](https://www.npmjs.com/package/check-node-version)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
